# sweep

## Usage

Just instance a `Field` and pass parameters.

| Parameter | Type | Role |
| --------- | ---- | ---- |
| `origin` | `Vector2` | Top left position of the `Field`
| `field_size` | `Vector2` | Size of the `Field` in `Tile`s |
| `mine_count` | `int` | Mine count |

## Changing Sprites

`Tile`s used in `Field` are categorised by numbers and sprites are loaded by the category.
To change the sprites used, you can add any 16x16 sprite and rename them to match category number.

For example, if `Tile`s code is 9 it means it has a mine and mine sprite is loaded.
To change the look of mine, replace `res://src/tile/9.png`

| Number | Category |
| ------ | -------- |
|-2| Has flag, tile is disabled
|-1| Closed
|0| Open, 0 mines around
|1| Open, 1 mine around
|...|
|8| Open, 8 mines around
|9| Has mine

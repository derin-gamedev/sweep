extends TileMap

signal game_ended
signal game_won

export(PackedScene) var Tile = preload("res://addons/sweep/src/tile/Tile.tscn")

export(Vector2) var origin
export(Vector2) var field_size
export(int) var mine_count

var mines_planted = false

func _ready():
	create_field()

func digged(tile_name: String):
	var tile_count = int(field_size.x * field_size.y)
	var digged_tiles = []
	for tile in get_children():
		if tile.digged:
			digged_tiles.append(tile)
	if digged_tiles.size() == tile_count - mine_count:
		emit_signal("game_won")

func clear_surrounding(tile_name: String):
	for surrounding_tile in get_undigged_surrounding_tiles(tile_name):
		if surrounding_tile.has_mine:
			continue
		else:
			surrounding_tile.clear()

func plant_mines(starter_tile: String):
	randomize()
	var tiles_with_mines = []
	while tiles_with_mines.size() < mine_count:
		var tile_name = get_tile_name(
			Vector2(
				randi() % int(field_size.x), 
				randi() % int(field_size.y)
			)
		)
		if tile_name == starter_tile:
			continue
		var starter_surrounding_tiles =  get_surrounding_tiles(starter_tile)
		if get_node(tile_name) in starter_surrounding_tiles:
			continue
		var unlucky_tile = get_node(tile_name)
		if unlucky_tile in tiles_with_mines:
			continue
		else:
			tiles_with_mines.append(unlucky_tile)

	for tile_with_mine in tiles_with_mines:
		tile_with_mine.has_mine = true
		tile_with_mine.set_content(9)

	for tile in get_children():
		if tile.has_mine:
			continue
		set_mine_count(tile.name)
	
	mines_planted = true

func create_field():
	for tile in get_children():
		tile.queue_free()
	
	for x in field_size.x:
		for y in field_size.y:
			var tile = Tile.instance()
			tile.name = get_tile_name(Vector2(x, y))
			tile.rect_position = field_to_world(tile.name)
			tile.field = self
			add_child(tile, true)
			set_cellv(field_to_world(tile.name), -1)



func set_mine_count(tile_name: String):
	var tile = get_node(tile_name)
	if tile.has_mine:
		return

	var mines = 0
	var surrounding_tiles = get_surrounding_tiles(tile_name)

	for surrounding_tile in surrounding_tiles:
		if surrounding_tile.has_mine:
			mines += 1

	tile.set_content(mines)


func get_surrounding_tiles(tile_name):
	var pos = tile_name_to_vector(tile_name)
	var tiles = []
	for x in [-1, 0, 1]:
		for y in [-1, 0, 1]:
			var relative_pos = Vector2(x, y)
			if relative_pos == Vector2.ZERO:
				continue
			var true_pos = pos + relative_pos
			if has_node(get_tile_name(true_pos)):
				tiles.append(get_node(get_tile_name(true_pos)))
	return tiles

func get_undigged_surrounding_tiles(tile_name):
	var pos = tile_name_to_vector(tile_name)
	var tiles = []
	for x in [-1, 0, 1]:
		for y in [-1, 0, 1]:
			var relative_pos = Vector2(x, y)
			if relative_pos == Vector2.ZERO:
				continue
			var true_pos = pos + relative_pos
			if has_node(get_tile_name(true_pos)):
				if not get_node(get_tile_name(true_pos)).digged:
					tiles.append(get_node(get_tile_name(true_pos)))
	return tiles
	

func get_tile_name(vec: Vector2):
	return var2str(int(vec.x)) + "," + var2str(int(vec.y))

func world_to_field(world_pos: Vector2):
	return world_to_map(world_pos - origin)

func tile_name_to_vector(tile_name: String):
	var splitted_name = tile_name.split(",")
	return Vector2(
		str2var(splitted_name[0]), 
		str2var(splitted_name[1])
	)

func field_to_world(tile_name: String):
	var tile_pos = tile_name_to_vector(tile_name)
	return map_to_world(tile_pos + origin)

extends TextureButton

signal tile_opened(position)

var has_mine = false
var digged = false
var field
var content = 0

var textures_path_base = ""

func _on_Tile_gui_input(event):
	if event is InputEventMouseButton and event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				if not digged and not disabled:
					dig()
			BUTTON_RIGHT:
				disabled = !disabled

func _ready():
	textures_path_base = get_script().get_path().get_base_dir() + "/"
	$Content.visible = false
	$BG.visible = false

func set_content(code: int):
	content = code
	var path = textures_path_base + var2str(code) + ".png"
	$Content.texture = load(path)

func show():
	$Content.visible = true
	$BG.visible = true

func clear():
	if digged:
		return
	digged = true
	show()
	if content == 0:
		field.clear_surrounding(name)

func dig():
	digged = true
	show()
	field.digged(name)
	if not field.mines_planted:
		field.plant_mines(name)
	if has_mine:
		field.emit_signal("game_ended")
		return
	if content == 0:
		field.clear_surrounding(name)


func _on_Tile_pressed():
	emit_signal("tile_opened", name)

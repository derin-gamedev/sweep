tool
extends EditorPlugin

func _enter_tree():
	add_custom_type(
		"Field",
		"TileMap",
		preload("res://addons/sweep/src/field/Field.gd"),
		preload("res://addons/sweep/img/Field.svg")
	)


func _exit_tree():
	remove_custom_type("Field")
